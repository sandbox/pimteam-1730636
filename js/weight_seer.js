var weightSeer={};

weightSeer.validateCalculator = function(frm)
{
	var age=frm.age.value;
	if(isNaN(age) || age<6 || age > 125 || age=="")
	{
		alert(wsText["Please enter your age, numbers only."]);
		frm.age.focus();
		return false;
	}
	
	var height_ft=frm.height_ft.value;
	if(isNaN(height_ft) || height_ft<=0) height_ft="";
	var height_in=frm.height_in.value;
	if(isNaN(height_in) || height_in<=0) height_in="";
	var height_cm=frm.height_cm.value;
	if(isNaN(height_cm) || height_cm<=0) height_cm="";
	
	if(height_ft=="" && height_cm=="" && height_in=="")
	{
		alert(wsText["Please enter your height, numbers only."]);
		return false;
	}
	
	var weight_lb=frm.weight_lb.value;
	if(isNaN(weight_lb) || weight_lb<=0) weight_lb="";
	var weight_kg=frm.weight_kg.value;
	if(isNaN(weight_kg) || weight_kg<=0) weight_kg="";
	
	if(weight_kg=="" && weight_lb=="")
	{
		alert(wsText["Please enter your weight, numbers only."]);		
		return false;
	}
	
	
	var lose_lb=frm.lose_lb.value;
	if(isNaN(lose_lb) || lose_lb<0) lose_lb="";
	var lose_kg=frm.lose_kg.value;
	if(isNaN(lose_kg) || lose_kg<0) lose_kg="";
	
	if(lose_kg=="" && lose_lb=="")
	{
		alert(wsText["Please enter how much weight you want to lose, numbers only."]);		
		return false;
	}
	
	if(document.getElementById('radioTime').checked)
	{
		var days=frm.days.value;
		if(isNaN(days) || days<0 || days=="")
		{
			alert(wsText["Please enter how many days you have to reach the goal, numbers only."]);
			frm.days.focus();
			return false;
		}
	}	
	
	if(document.getElementById('radioCalories').checked)
	{
		var caloriesLess=document.getElementById('caloriesDiet').value;
		if(isNaN(caloriesLess) || caloriesLess<0 || caloriesLess=="")
		{
			alert(wsText["Please enter how many calories you are willing to reduce from your daily intake. Numbers only."]);
			frm.calories.focus();
			return false;
		}
	}	
}

weightSeer.calculateHeight = function(fld)
{
	if(fld.name=="height_in" || fld.name=="height_ft")
	{
		// calculate height in inches
		if(isNaN(fld.form.height_in.value) || fld.form.height_in.value=="") inches=0;
		else inches=fld.form.height_in.value;
		
		if(isNaN(fld.form.height_ft.value) || fld.form.height_ft.value=="") feet=0;
		else feet=fld.form.height_ft.value;
		
		inches=parseInt(parseInt(feet*12) + parseInt(inches));
		
		h=Math.round(inches*2.54);
		
		fld.form.height_cm.value=h;
	}
	else
	{
		// turn cm into feets and inches
		if(isNaN(fld.value) || fld.value=="") cm=0;
		else cm=fld.value;
		
		totalInches=Math.round(cm/2.54);
		inches=totalInches%12;		
		feet=(totalInches-inches)/12;
		
		fld.form.height_ft.value=feet;
		fld.form.height_in.value=inches;
	}
}

weightSeer.calculateWeight = function(fld)
{
	if(fld.name=="weight_lb" || fld.name=="lose_lb")
	{
		// calculate in kg
		if(isNaN(fld.value) || fld.value=="") w=0;
		else w=fld.value;
		
		wKg=Math.round(w*0.453*10)/10;
		
		if(fld.name=="weight_lb") fld.form.weight_kg.value=wKg;
		else fld.form.lose_kg.value=wKg;
	}
	else
	{
		// calculate in lbs
		if(isNaN(fld.value) || fld.value=="") w=0;
		else w=fld.value;
		
		wP=Math.round(w*2.2);
		
		if(fld.name=='weight_kg') fld.form.weight_lb.value=wP;
		else fld.form.lose_lb.value=wP;
	}
}