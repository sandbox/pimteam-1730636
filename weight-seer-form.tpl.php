<?php
/**
 * @file
 * Theme file to display the form.
 */
?>
<noscript>WeightSeer requires javascript to be enabled to work.</noscript>

<div id="weight-seer-wrapper">
	<form method="post" onsubmit="return weightSeer.validateCalculator(this);">
		<fieldset>
		<label><?php echo t("Your age:");?> <?php print drupal_render($form['age']); ?></label>
		<label><?php echo t("Your gender:");?> <?php print drupal_render($form['gender']); ?></label>
		</fieldset>
		
		<fieldset>
		<div><label><?php echo t("Your height:");?></label> <?php print drupal_render($form['height_ft']); ?> <?php echo t('ft')?> &amp; <?php print drupal_render($form['height_in']); ?> <?php echo t('in');?> <strong><?php t("OR")?> </strong> <?php print drupal_render($form['height_cm']); ?> <?php echo t("cm")?></div>	
		</fieldset>
		
		<fieldset>
		<div><label><?php echo t("Your weight")?>:</label> <?php print drupal_render($form['weight_lb']); ?> <?php echo t("lbs")?> <strong><?php t("OR")?> </strong> <?php print drupal_render($form['weight_kg']); ?> <?php echo t("kg")?></div>
		</fieldset>

		<fieldset>		
		<div><label><?php echo t("Daily activity level")?>:</label> <?php print drupal_render($form['activity']); ?></div>
		</fieldset>

		<fieldset>		
		<div><label class="clear"><?php echo t("How much weight you wish to lose?")?>:</label> <?php print drupal_render($form['lose_lb']); ?> <?php echo t("lbs")?> <strong><?php t("OR")?> </strong> <?php print drupal_render($form['lose_kg']); ?> <?php echo t("kg")?></div>
		</fieldset>
		
		<fieldset>
		<div><label class="clear"><?php print drupal_render($form['mode_time']); ?> <?php echo t("How much time do you have?")?></label>
			<?php print drupal_render($form['days']); ?> <?php echo t("days");?>
			<br><?php echo t("(In this case I'll calculate your required daily calorie intake)")?></div>
		</fieldset>	
		
		<fieldset>	
		<div><label class="clear"><?php print drupal_render($form['mode_calories']); ?> <?php echo t("How many calories are you ready to reduce?")?></label>
			<?php print drupal_render($form['calories']); ?> <?php echo t("calories per day");?>
			<br>
			<?php echo t("(In this case I'll calculate the approximate date when you will achieve your goal)")?></div>	
		</fieldset>
			
		<div><?php print drupal_render($form['submit']); ?></div>	
		
		<?php print drupal_render_children($form); ?>
	</form>	
	
	<script type="text/javascript" >
	var wsText={};
	wsText["Please enter your age, numbers only."]="<?php echo t('Please enter your age, numbers only.')?>";
	wsText["Please enter your height, numbers only."]="<?php echo t('Please enter your height, numbers only.')?>";
	wsText["Please enter your weight, numbers only."]="<?php echo t('Please enter your weight, numbers only.')?>";
	wsText["Please enter how much weight you want to lose, numbers only."]="<?php echo t('Please enter how much weight you want to lose, numbers only.')?>";
	wsText["Please enter how many days you have to reach the goal, numbers only."]="<?php echo t('Please enter how many days you have to reach the goal, numbers only.')?>";
	wsText["Please enter how many calories you are willing to reduce from your daily intake. Numbers only."]="<?php echo t('Please enter how many calories you are willing to reduce from your daily intake. Numbers only.')?>";
	</script>
</div>