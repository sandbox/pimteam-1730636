CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Current Maintainer: Bob Hands <admin@pimteam.net>

Weight Seer is a small module that renders a functional fitness/weight goal
calculator and estimator on your site. The module allows visitors to 
figure out how to reach a given weight target depending on a given timeframe
or projected calories intake.


INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.

The most apropriate place to include the module is in a selected page in the "Content" 
(section and obviously not on all pages in this case). Another option is to include 
it on a sidebar and have it displayed across the entire site.