<?php
/**
 * @file
 * Theme file to display the form.
 */
?>
<div id="weight-seer-wrapper">

	<?php if($message['high_risk_weight']): ?>
		<div class="ws_warning"><?php echo t('Warning: your goal requires you to lose ').number_format($message['pounds_daily']*7).t(' pounds per week. This implies a high risk for your health and is not recommended!');?></div>
	<?php endif;
	if($message['high_risk_calories']):?>
		<div class="ws_warning"><?php echo t('Warning: your goal requires you to lose ').number_format($message['calories_less']).t(' calories per day, which means you are supposed to intake only ').number_format($message['calories_daily']).t(' calories daily. This implies a high risk for your health and is not recommended!')?></div>
	<?php endif; ?>
	
	<?php echo $message['message']; ?>
	
	<p align='center'><?php echo l('Calculate again',current_path(),array("query"=>array("weight_seer_unset"=>1)))?></p>	
</div>